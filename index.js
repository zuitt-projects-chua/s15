/*alert("Hello")*/

/*
Assignment Operators

*/

let num1 = 5;
let num2 = 10;
let num3 = 15;
let num4 = 20;

num1 += num4
console.log(num1);
//addition operation in numbers result is num1 = 25

let string1 = "Charles";
let string2 = "Chua";

string1 += string2;
console.log(string1);
//concatenation on strings

num4 -= num1;
console.log(num4);
//result is num4=-5 , the value is updated

num4 -= 10;
console.log(num4);
//result num4 = -10

num1 -= string1;
console.log(num1);
//result NaN  Not a number

num2 *= num3;
console.log(num2);
//result 150

num2 *= 3;
console.log(num2);

num2 /= num4;
console.log(num2);

num2 /= -3;
console.log(num2);

//Mathematical Operation - MDAS in Javascript

let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult);
/*
3*4=12
12/5=2.4
1+2=3
3-2.4=0.6

Pemdas parenthesis exponent
*/

//Increment and Decrement
//Pre-fix and Post-fix

let z = 1;
++z;
console.log(z);

z++;
console.log(z)
// result 3 the value z is added with 1

console.log(z++)
//result 3, with post increment the previous value was return before increment

console.log(z)
//result 4 the new value is returned

//Pre-fix and Post-fix Decrementation
console.log(--z);
//result 3 the reuslt is returned immediately

console.log(z--);
//return previous value

console.log(z);
//return new value


//Comparison Operators returns a boolean

//Equality Operand
console.log(1==1);

let isSame = 55==55;
console.log(isSame);

console.log(1=="1");
//loose equality operand prioritize the same value regardless of datatype

console.log(0==false);
//false is converted to a number

console.log(1==true);
console.log(true=="true")

/*
With loose comparison operator (==), the values are compared and types, if operand doesnt have same type, it will be forced coerced before comparison

*/

//Strict Equality Operator (===)

console.log(true==="1");
console.log("Johnny" === "Johnny")
console.log("lisa" === "Lisa")

//Loose Inequality Operators

console.log("1" != 1);

//Strict Inequality Operators

console.log("5" !== 5);
console.log(5 !== 5);
console.log("true" !== true);

let name1 = "Juan";
let name2 = "Shane";
let name3 = "Peter";
let name4 = "Jack";

let number = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log("Activity:")
console.log(numString1 == number);
console.log(numString1 === number );
console.log(numString1 != number);
console.log(name4 !== name3);
console.log(name1 == "juan");
console.log(name1 === "Juan");

//Relational Comparison Operators
//comparison operator which check the relationship between operands

let q = 500;
let r = 700;
let w = 8000;
let numString3 = "5500";


console.log(q>r);
console.log(w>r);
console.log(w<q);
console.log(q<1000);
console.log(numString3 < 6000);
console.log(numString3 < "Jose"); //result eratic

//Greater than or Equal >=

console.log(w >= 8000);
console.log(r >= q);
console.log(q <= r);
console.log(w <= q);

//Logical Operators
/*
And operators (&&)
both operands added must be true or result to true

*/

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 =isAdmin && isRegistered;
console.log(authorization1);

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && (requiredLevel===25);
console.log(authorization3);

let authorization4 = isRegistered && isLegalAge && requiredLevel===95;
console.log(authorization4);

let userName = "gamer2001";
let username2 = "shadow1991";
let userAge1 = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge1 >= requiredAge;
console.log(registration1);

let registration2 = username2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

/*
Or Operator ||

Operator returns true if at least one of them are true

*/

let userLevel = 100;
let userLevel2 = 65;
let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge1 >=requiredAge;

console.log(guildRequirement);

let guildRequirement2 = isRegistered || userLeve2 >= requiredLevel || userAge2 >=requiredAge;
console.log(guildRequirement2);

let guildRequirement3 = isAdmin || userLevel >= requiredLevel;
console.log(guildRequirement3);

/*
Not Operator
turns a boolean value into the opposite value
*/

console.log(!isRegistered);

/*
If-else Statements

if statement - will run a block of code if the condition specified is true
*/

/*if (true){
	alert('We run an if condition!')
};*/

let username3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(username3.length > 10){
	console.log("Welcome to Game Online")
};

if(userLevel3 >= requiredLevel){
	console.log("You're qualified!");
};

if (username3.length >= 10 && isRegistered && isAdmin){
	console.log("Thank you")
};

/*
Else statement
will run if the condition given is false

*/

if(username3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the guild!")
} else {
	console.log("You are too strong to be a noob")
};

/*
Else if 

executes a statement if the original condition is false but another specified condition is true
*/

if (username3 ){
	console.log("Thanky you for joining the guild")
} else if (userLevel3 > 25 ) {
	console.log("you are too strong");
} else if (userAge3 < requiredAge ) {
	console.log("Too young to join");
} else if (username3.length < 10) {
	console.log("Username is too short");
}


//if-else inside a function

function addNum (num1, num2) {
	if (typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both are numbers");
		console.log(num1 + num2);
	} else {
		console.log("One or both of arguments are not numbers");
	}

}

addNum (5, "two")

function login (username, password){
	if (typeof username === "string" && typeof password === "string"){
		console.log("Both arguments are string");
		if (username.legth >= 8 && password.length >= 8){
			alert("Thank you for logging in")
		} else if (username.length < 8){
			alert("Username too short");
		} else if (password.length <8 ){
			alert("Password to short");
		}
	} else {
		console.log("One of the arguments are not string type.")
	}
}

login("theTinker", "tinkerbell");

/*
Switch Statements
alternative to an if, else-if , else tree, where the data is being evaluated or checked 

*/

let hero = "Hercules";

switch(hero){
	case "Jose Rizal":
	console.log("National Hero of Philippines");
	break;

	case "George Washington":
	console.log("Hero of the American revolution");
	break;

	case "Hercules":
	console.log("Legendary Hero of the Greek");
	break;
};

function roleChecker (role){
	switch (role){
		case "Admin":
		console.log("Welcome! Admin");
		break;

		case "User":
		console.log("Welcome! User");
		break;

		case "Guest":
		console.log("Welcome! Guest");
		break;

		default:
		console.log("Invalid role!");

	}
};

roleChecker(1)

function gradeEvaluator (grade){
	if (grade >= 90){
		return "A"
		//console.log("A is your grade")
	} else if (grade >= 80){
		return "B"
	}else if (grade >= 71){
		return "C"
	} else if (grade <= 70){
		return "F"
	} else {
		return "Invalid Grade"
	}
};

let letterDistinction = gradeEvaluator (93);
console.log(letterDistinction);

/*
Ternary Operator
A shorthand way of writing if-else 
*/

let price = 5000;
price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000")

let villain = "Harvey Dent";
/*villain === "Harvey Dent" ? console.log("You were supposed to be chosen one.");
Else statement in ternary operator is required
Ternary operator are not used to complex/ nested if else

*/

villain === "Two Face"
? console.log("You lived long enough to be a villain")
: console.log("Not quite a villainous yet")

//Else if with Ternary Operator

let a = 7;

a === 5
? console.log("A")
:console.log(a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"));



